const express = require('express');
const app = express();
const port = 3000;

app.use(express.static('public'));

app.get('/', function(req, res) {
    res.redirect('/home');
})

app.get('/cart', function (req, res) {
    res.sendFile('public/cart.html', { root: __dirname })
})

app.get('/home', function (req, res) {
    res.sendFile('public/home.html', { root: __dirname })
})

app.get('*', function(req, res) {
    res.sendFile('public/404.html', { root: __dirname })
})

app.listen(port, function () {
    console.log('Сервер запущен')
})